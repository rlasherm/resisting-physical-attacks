# Resisting Physical Attacks

Running an application with security functions on an embedded system requires facing a new threat: physical attacks.
The attacker has physical access to the system running your application, offering numerous possibilities: measuring execution times, listening to the electromagnetic environment, or interacting directly with the circuit...

## Your Mission

You have been tasked with developing a PIN verification code for a microcontroller. The system receives 4 digits from 0 to 9 and must compare them to those, secret, stored on the chip.

You have been prudent, and your chip's memory is TIP TOP SECURE: the attacker cannot read it once the secret code is present. However, they do have a copy of your application, without the secret code, which allows them to prepare their attacks.

Your adversary will do everything to recover this secret, will you be able to counter them?

## Report

For this practical session, you will need to write a report.
It should be structured as follows:
- Introduction: scenario and technologies used for the practical session.
- For each attack from B to H:
    - A description of the attack
    - A description of the countermeasures for this attack
    - A discussion, when necessary, on the potential weaknesses of the countermeasures, on a possible improvement of the attack, etc.
- A description and a qualitative evaluation of your final implementation: which countermeasures are implemented, what weaknesses remain, etc.
- A conclusion of the practical session. (Feel free to comment on the practical session itself, points to improve, etc.).

## Setting Up the Working Environment

Before coding, you will need to set up the working environment:

- A cross-compilation chain to compile code for the microcontroller from your PC.
- *Python* and some libraries that will be used to emulate the chip and simulate the attacks.
- Use your favorite code editor (emacs, of course).

### Linux

#### Automatic Installation (recommended)

Install the Nix package manager (requires sudo permissions, and *curl*).

```
curl -L https://nixos.org/nix/install | sh
```

Follow the on-screen instructions to complete the installation.

Use the file *exercises/default.nix* as the Nix configuration, for example,

```
cd exercises
nix-shell
```
This gives you a bash shell with everything you need to do the exercises, without modifying your system (in particular, no need for virtualenv for python).

(default.nix is the default file searched for by nix-shell)

#### Manual Installation (because you really, really do not want to use Nix)

Start by installing the cross-compilation chain (commands given with apt for Debian-like systems).
```
sudo apt install gcc-arm-none-eabi
```

Then install python, its package manager (pip), and virtualenv.
```
sudo apt install python3
sudo apt install python3-pip
pip3 install virtualenv
```

Now, set up a virtualenv environment (the executable must be in PATH).
```
virtualenv exosenv
```

Finally, before each work session, activate the environment:

```
source exosenv/bin/activate
```

**At the end of the session**, deactivate the environment with

```
deactivate
```


Within a session (after the source and before the deactivate), install the python libraries.
```
pip3 install unicorn
pip3 install numpy
pip3 install pyelftools
pip3 install termcolor
```

For lesson 4, you will also need to install deep learning libraries. Be aware, they are large.
```
pip3 install tensorflow
pip3 install keras
```

## Course Progression

The skeleton of your PIN verification application is located in the **pin_verif** folder; this is the one you will modify to obtain the final application.

There are 4 lessons, from L1 to L4, addressing different issues, to be completed in order:
- L1: First implementation of a naïve PIN code.
- L2: Tearing and time dependence (or when time is your concern).
- L3: Fighting against fault injection attacks.
- L4: Fighting against observation attacks.

For each lesson, follow the instructions in the **Readme.md** of the lesson's folder.