# Résister aux attaques physiques

Ce dépot contient les ressources pour deux cours permettant de comprendre et d’apprendre à ce prémunir des attaques physiques.
- [PIN](./exercices/PIN) est un cours pour apprendre à développer une application simplifiée de vérification de code  PIN.
- [AES](./exercices/AES) est un cours qui permet de retrouver les clés de chiffrement AES à partir d’attaques en fautes ou par canaux auxiliaires.
