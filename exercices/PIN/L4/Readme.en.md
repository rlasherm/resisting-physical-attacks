# Attacker Model

An observational attack measures the physical environment of the circuit to try to gain information about the computation taking place. This often involves measuring power consumption or electromagnetic radiation.

In this exercise, the information leakage will be simulated: the attacker is capable of recovering the sum of all the internal registers of the chip for each instruction of *verify_pin* and *compare_arrays* (the sum, not the individual values).

## Learning-Based Attack

We will use a simple neural network to learn the link between the measured traces and the secret PIN present in the card. Thus, the attack assumes that the attacker has access to an identical chip to the target on which they can measure whatever they want. In particular, they are capable of changing the secret PIN on their card and making as many attempts as they want (it's enough to enter the correct PIN every 2 measurements).

## Work Environment

### With Nix

Use the Nix configuration from the L4 directory:

```bash
nix-shell ./L4/default.nix
```

### Manual Installation of Necessary Packages

The python packages keras and tensorflow must be installed for this exercise.

```
pip3 install tensorflow
pip3 install keras
```

## Launching the Attack

This attack requires a lot of resources; a powerful PC is recommended, as simulating enough PIN code verifications can be lengthy (between 30 minutes and hours...).

The first execution will initiate the learning.
```
python3 ../L4/H-deep-learning.py .
```

To reload the previous learning, use `--load`.

```
python3 ../L4/H-deep-learning.py . --load
```

## Countermeasure

But then, how can one protect against such an attack?