# Differential Fault Analysis

Cet exercice a pour but de générer des fautes exploitables permettant de récupérer la clé AES.


## Trouver où faire la faute

Nous allons simuler l'injection de faute, habituellement obtenue à l'aide d'un glitch sur l'horloge, ou via une impulsion électromagnétique.
Le modèle de faute est le saut d'instruction avec biais, très proche de ce que l'on obtient réellement expérimentalement.

Une faute sur l'implémentation AES au hasard n'a que peux de chance d'être exploitable. Il faut la faire à un moment précis dans l'algorithme, en fonction du modèle de faute.
Ici nous vous proposons d'implémenter l'attaque [NUEVA](./Docs/FDTC2012.pdf) (Non-Uniform Error Value Analysis).
Alternativement, il est possible d'implémenter l'attaque [PQ](./Docs/PQ.pdf) (Piret-Quisquater) avec notre modèle de faute, mais la quantité de calcul nécessaire, $4 \cdot 2^{32}$ hypothèses de clé, est élevé.

La première étape est de trouver où et quand faire la faute, sous la forme de deux paramètres :
- l'adresse de la faute en mémoire.
- au bout de combien d'exécutions de cette adresse, doit-on injecter la faute.

Pour cela, vous pouvez utiliser le script *K-fault-calibration.py* comme suit.

```
python3 ./L6-DFA/K-fault-calibration.py ./aes_encrypt 3 MixColumns
```

L'injection sera testée pour une même paire de texte clair, clé pour chaque instruction de la fonction "MixColumns", au bout de la 3e exécution de cette instruction (si l'instruction n'est exécutée que deux fois, elle ne sera jamais fautée).
Les noms de fonctions correspondent aux noms dans le code C et leurs versions compilées peuvent être observées dans le fichier [aes_encrypt/bin/aes.list](../aes_encrypt/bin/aes.list).

Un exemple de résultat :
```
python3 ./L6-DFA/K-fault-calibration.py ./aes_encrypt 3 MixColumns
Faulting address 0x8000426 : ** Crash **
Faulting address 0x8000428 : ** Crash **
Faulting address 0x800042a : ** Crash **
Faulting address 0x800042c : ! Fault detected ! Diff: 3925841d02dc09fbdc118597196a0b32	Output: 00000000000000000000000000000000
Faulting address 0x800042e : ! Fault detected ! Diff: 1a9443f7e45456e225f38f506f26e459	Output: 23b1c7eae6885f19f9e20ac7764cef6b
Faulting address 0x8000430 : No fault
Faulting address 0x8000432 : ! Fault detected ! Diff: 3da295222b43472b8386a8769bff8e4a	Output: 0487113f299f4ed05f972de182958578
Faulting address 0x8000434 : No fault
Faulting address 0x8000436 : ! Fault detected ! Diff: 2655b932143a10a2736d0f8e3aba4997	Output: 1f703d2f16e61959af7c8a1923d042a5
Faulting address 0x8000438 : ! Fault detected ! Diff: 88044f2d1f39f9896929236d7cfb5fb4	Output: b121cb301de5f072b538a6fa65915486
Faulting address 0x800043a : No fault
Faulting address 0x800043c : ! Fault detected ! Diff: c49d3d0d45f9b34cfd79e859ceb39def	Output: fdb8b9104725bab721686dced7d996dd
Faulting address 0x800043e : ! Fault detected ! Diff: 7cb552f63bfc812c54d94d9700c1aef8	Output: 4590d6eb392088d788c8c80019aba5ca
Faulting address 0x8000440 : No fault
Faulting address 0x8000442 : ! Fault detected ! Diff: 36e2ed5e5fdb6b47e758bbcac9d570c4	Output: 0fc769435d0762bc3b493e5dd0bf7bf6
Faulting address 0x8000444 : ! Fault detected ! Diff: a24a9b4b84ffba1f4d97fa2016d3f680	Output: 9b6f1f568623b3e491867fb70fb9fdb2
Faulting address 0x8000446 : No fault
Faulting address 0x8000448 : ** Crash **
Faulting address 0x800044a : ! Fault detected ! Diff: d5478fad02deb5f5c626aac5759a8fdd	Output: ec620bb00002bc0e1a372f526cf084ef
Faulting address 0x800044c : No fault
Faulting address 0x800044e : ! Fault detected ! Diff: daf7117d5e80d8e47bd98474f0180c64	Output: e3d295605c5cd11fa7c801e3e9720756
Faulting address 0x8000450 : No fault
Faulting address 0x8000452 : ! Fault detected ! Diff: c664ac4a2735a88d60c5d4dd28533c2d	Output: ff41285725e9a176bcd4514a3139371f
Faulting address 0x8000454 : ! Fault detected ! Diff: 28574005fc0a6fb3a5c9d58bb72e102c	Output: 1172c418fed6664879d8501cae441b1e
Faulting address 0x8000456 : No fault
Faulting address 0x8000458 : ! Fault detected ! Diff: f50956b6a3668805584119d720b92a7b	Output: cc2cd2aba1ba81fe84509c4039d32149
Faulting address 0x800045a : ! Fault detected ! Diff: b04dbb3ffcb013b9c2e54a20d26d1cda	Output: 89683f22fe6c1a421ef4cfb7cb0717e8
Faulting address 0x800045c : No fault
Faulting address 0x800045e : ! Fault detected ! Diff: 205a65938296d607feff2f237bd7d993	Output: 197fe18e804adffc22eeaab462bdd2a1
Faulting address 0x8000460 : ! Fault detected ! Diff: 92d2e0109e19766f46ebe2718ea7f39c	Output: abf7640d9cc57f949afa67e697cdf8ae
Faulting address 0x8000462 : No fault
Faulting address 0x8000464 : ! Fault detected ! Diff: 35cc734e5e74b6983b9737c0c895526a	Output: 0ce9f7535ca8bf63e786b257d1ff5958
Faulting address 0x8000466 : No fault
Faulting address 0x8000468 : ! Fault detected ! Diff: da8185c3ac3fe20c61880d4c1ff263d5	Output: e3a401deaee3ebf7bd9988db069868e7
Faulting address 0x800046a : ** Crash **
Faulting address 0x800046c : ** Crash **
```

Lorsque vous aurez identifié l'adresse et l'itération à fauter, vous pourrez lancer l'attaque par injection de fautes proprement dite.

## DFA

L'attaque se fait via le script *L-DFA.py* en l'appelant comme suit

```
python3 ./L6-DFA/L-DFA.py [target folder] [fault address] [execution count] [iteration]
```

par exemple

```
python3 ./L6-DFA/L-DFA.py ./aes_encrypt  0x812345 1000 3
```
va générer les fichiers *faulty_ciphertexts.npy* et *ciphertexts.npy* contenant respectivement les chiffrés fautés et corrects correspondant.

À vous de jouer ! Grâce à ces fautes, à vous de retrouver la clé.
