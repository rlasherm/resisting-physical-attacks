#!/usr/bin/env python
# Sample code for ARM of Unicorn. Nguyen Anh Quynh <aquynh@gmail.com>
# Python sample ported by Loi Anh Tuan <loianhtuan@gmail.com>

from unicorn import *
from unicorn.arm_const import *

from termcolor import colored, cprint

import sys
import os
import secrets

import binascii
from elftools.elf.elffile import ELFFile
from elftools.elf.sections import NoteSection, SymbolTableSection

from Crypto.Cipher import AES

# we read the elf file to automatically extract the address and size of the given symbol
def extract_symbol_range(symbol_name, elf_path):
    e = ELFFile(open(elf_path, 'rb'))
    symbol_tables = [s for s in e.iter_sections() if isinstance(s, SymbolTableSection)]
    for section in symbol_tables:
        for symbol in section.iter_symbols():
            if symbol.name == symbol_name:
                sstart = symbol['st_value'] & 0xFFFFFFFE
                send = symbol['st_size'] + symbol['st_value'] & 0xFFFFFFFE

                return range(sstart, send)

def extract_symbol_address(symbol_name, elf_path):
    return extract_symbol_range(symbol_name, elf_path).start

def parse_input_bytes(input_str):
    return bytes.fromhex(input_str)

def display_bytes(input):
    return input.hex() 

# code to be emulated
def load_code(path):
    return open(path, 'rb').read()

def init_nvm():
    content = bytearray([0 for _ in range(1024)])
    return bytes(content)

# Start with a card already locked
nvm_content = init_nvm()

MAX_FAULTS = 1
FAULT_AFTER = 0
FAULT_FUNCTION = ""

class AESEmulator:

    # memory address where emulation starts
    INST_ADDRESS    = 0x8000000
    RAM_ADDRESS     = 0x20000000
    IO_ADDRESS      = 0x10000000
    NVM_ADDRESS     = 0x50000000

    PLAINTEXT_ADD   = 0x10000000
    KEY_ADD         = 0x10000020
    CIPHERTEXT_ADD  = 0x10000040

    # self.fault_count = 0

    def __init__(self, target_app_folder_path):
        self.bin_path = target_app_folder + '/bin/aes.bin'
        self.elf_path = target_app_folder + '/bin/aes.elf'
        self.THUMB_CODE = load_code(self.bin_path)

    def skip_at(self, uc, skip_address):
        uc.hook_add(UC_HOOK_CODE, self.hook_skip, None, skip_address, skip_address)

    def hook_skip(self, uc, address, isize, user_data):
        if address in self.injection_history:
            self.injection_history[address] += 1
        else:
            self.injection_history[address] = 1

        # print(">>> Skip instruction at 0x%x, instruction size = 0x%x -> new PC = 0x%x" %(address, isize, (address + isize)))
        if self.fault_count < MAX_FAULTS and self.injection_history[address] == FAULT_AFTER:
            uc.reg_write(UC_ARM_REG_PC, (address + isize) | 1)
            self.fault_count += 1

    # callback for tracing instructions
    # def hook_code(self, uc, address, size, user_data):
    #     print(">>> Tracing instruction at 0x%x" %(address))

    # def execute(self, fault_address, plaintext, key):
    def execute(self, fault_address, plaintext, key):
        global nvm_content
        self.fault_count = 0
        self.injection_history = dict()
        try:
            # Initialize emulator in thumb mode
            mu = Uc(UC_ARCH_ARM, UC_MODE_THUMB)

            # map 128kiB of flash memory for the code
            mu.mem_map(self.INST_ADDRESS, 128 * 1024)
            # map 8kiB of ram memory
            mu.mem_map(self.RAM_ADDRESS, 8 * 1024)
            # map 1kiB for Non Volatile Memory (NVM)
            mu.mem_map(self.NVM_ADDRESS, 1 * 1024)
            # map 1kiB for IO buffer
            mu.mem_map(self.IO_ADDRESS, 1 * 1024)

            # write machine code to flash
            mu.mem_write(self.INST_ADDRESS, self.THUMB_CODE)

            #write nvm content
            mu.mem_write(self.NVM_ADDRESS, nvm_content)

            # tracing one instruction at ADDRESS with customized callback
            # aes_range = extract_symbol_range("aes_encrypt", self.elf_path)
            # mu.hook_add(UC_HOOK_CODE, self.hook_code, begin=aes_range.start, end=aes_range.stop)

            # where to inject fault
            self.skip_at(mu, fault_address)

            #write inputs
            mu.mem_write(self.PLAINTEXT_ADD,plaintext)
            mu.mem_write(self.KEY_ADD, key)
            # mu.mem_write(self.INPUT_ADD, bytes([3,1,4,1]))

            # emulate machine code until timeout
            # Note we start at INST_ADDRESS | 1 to indicate THUMB mode.
            # mu.emu_start( start_address, end_address, timeout, nb_steps)
            mu.emu_start(self.INST_ADDRESS | 1, extract_symbol_address("_exit", self.elf_path), 100000, 100000)
            ciphertext = mu.mem_read(self.CIPHERTEXT_ADD, 16)

            return ciphertext

        except UcError as e:
            # print("ERROR: %s" % e)
            return bytes([])
 
def fault_range(address_range, target_app_folder_path, plaintext, key, expected_ciphertext):    
    for add in address_range:
        if add % 2 == 0:
            print("Faulting address 0x%x : " %add, end='')
            em = AESEmulator(target_app_folder_path)
            ciphertext = em.execute(add, plaintext, key)

            diff = differentiate(ciphertext, expected_ciphertext)

            clen = len(ciphertext)
            if clen != 16:
                cprint("** Crash **", 'red')
            elif ciphertext != expected_ciphertext:
                cprint("! Fault detected ! Diff: %s\tOutput: %s" %(display_bytes(diff), display_bytes(ciphertext)), 'yellow')
            else:
                cprint("No fault", 'green')

def aes_check(plaintext, key):
    alg = AES.new(key, AES.MODE_ECB)
    return alg.encrypt(plaintext)

def differentiate(a, b):
    return bytes([i ^ j for (i, j) in zip(a, b)])

if __name__ == '__main__':
    target_app_folder = '.'

    if len(sys.argv) < 4:
        print("Usages: python3 K-fault-calibration.py [target] [iteration] [symbol]")
        exit()

    target_app_folder = sys.argv[1]

    target_app_folder = os.path.abspath(target_app_folder)
    elf_path = target_app_folder + '/bin/aes.elf'

    FAULT_AFTER = int(sys.argv[2])
    FAULT_FUNCTION = sys.argv[3]

    # key = secrets.token_bytes(16)
    # plaintext = secrets.token_bytes(16)

    plaintext   = parse_input_bytes("3243f6a8885a308d313198a2e0370734")
    key         = parse_input_bytes("2b7e151628aed2a6abf7158809cf4f3c")
    # expected    = parse_input_bytes("3925841d02dc09fbdc118597196a0b32")
    expected = aes_check(plaintext, key)

    fault_range(extract_symbol_range(FAULT_FUNCTION, elf_path), target_app_folder, plaintext, key, expected)
    # fault_range(extract_symbol_range("verify_pin", elf_path), target_app_folder)
