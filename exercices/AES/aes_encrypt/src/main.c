/*
 * File: main.c
 * Project: target_app
 * Created Date: Monday May 25th 2020
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 27th November 2020 11:40:42 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2020 INRIA
 */


#include "aes.h"

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define PLAINTEXT_ADD   0x10000000
#define KEY_ADD         0x10000020
#define CIPHERTEXT_ADD  0x10000040

void main(void) {  
    aes_encrypt((void*)CIPHERTEXT_ADD, (void*)PLAINTEXT_ADD, (void*)KEY_ADD);
}
