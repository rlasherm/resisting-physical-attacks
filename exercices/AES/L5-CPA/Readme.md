# Correlation Power Analysis

Dans cette leçon, la consommation de courant de la puce sera simulée et vous devrez utilisés ces traces, ces mesures, pour retrouver la clé de chiffrement.
Pour cela vous devrez mettre en œuvre une Correlation Power Analysis (CPA).

## Génération des traces

Les traces sont simulées à l'aide du script *M-DFA.py* fournit.
```
python3 ./L5-CPA/M-CPA.py [target folder] [execution count] [symbol name]
```

Par exemple
```
python3 ./L5-CPA/M-CPA.py ./aes_encrypt 1000 MixColumns
```

Le nom du symbole doit être présent dans votre code, ce qui peut être vérifié dans le fichier [aes.list](../aes_encrypt/bin/aes.list)

Deux fichiers seront générés dans le répertoire courant.
*traces.npy* contient la matrice des traces au format numpy.
```
traces = numpy.load("traces.npy")
```
Un appel à *load* permet de charger les traces dans un tableau numpy à deux dimensions (une ligne par exécution, une colonne par instant de mesure).
Le deuxième fichier, *ciphertexts.npy* contient les chiffrés (une ligne par exécution, une colonne par octet).

## Exploitation

À vous de jouer ! Grâce aux traces et aux chiffrés, vous devez retrouver la clé.
