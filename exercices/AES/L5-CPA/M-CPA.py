#!/usr/bin/env python
# Sample code for ARM of Unicorn. Nguyen Anh Quynh <aquynh@gmail.com>
# Python sample ported by Loi Anh Tuan <loianhtuan@gmail.com>

from unicorn import *
from unicorn.arm_const import *

from termcolor import colored, cprint

import sys
import os
import secrets
import random
import numpy as np
from math import sqrt
from scipy import special

from gmpy2 import popcount

import binascii
from elftools.elf.elffile import ELFFile
from elftools.elf.sections import NoteSection, SymbolTableSection

from Crypto.Cipher import AES

sqrt2 = sqrt(2)

def noise(sigma):
    return sigma * sqrt2 * special.erfc(2.0*np.random.rand()-1.0)

# we read the elf file to automatically extract the address and size of the given symbol
def extract_symbol_range(symbol_name, elf_path):
    e = ELFFile(open(elf_path, 'rb'))
    symbol_tables = [s for s in e.iter_sections() if isinstance(s, SymbolTableSection)]
    for section in symbol_tables:
        for symbol in section.iter_symbols():
            if symbol.name == symbol_name:
                sstart = symbol['st_value'] & 0xFFFFFFFE
                send = symbol['st_size'] + symbol['st_value'] & 0xFFFFFFFE

                return range(sstart, send)

def extract_symbol_address(symbol_name, elf_path):
    return extract_symbol_range(symbol_name, elf_path).start

def parse_input_bytes(input_str):
    input_str = input_str.replace('0x', '')
    return bytes.fromhex(input_str)

def display_bytes(input):
    return input.hex() 

# code to be emulated
def load_code(path):
    return open(path, 'rb').read()

def init_nvm():
    content = bytearray([0 for _ in range(1024)])
    return bytes(content)

arm_regs = [UC_ARM_REG_SP, UC_ARM_REG_R0, UC_ARM_REG_R1, UC_ARM_REG_R2, UC_ARM_REG_R3, UC_ARM_REG_R4, UC_ARM_REG_R5, UC_ARM_REG_R6, UC_ARM_REG_R7, UC_ARM_REG_R8, UC_ARM_REG_R9,
            UC_ARM_REG_R10, UC_ARM_REG_R11, UC_ARM_REG_R12, UC_ARM_REG_R13, UC_ARM_REG_R14, UC_ARM_REG_R15]

def leakage_model(uc):
    total_hw = 0
    for r in arm_regs:
        val = uc.reg_read(r)
        total_hw += popcount(val)
        # total_hw += val
    return round(total_hw + noise(5.0))

# Start with a card already locked
nvm_content = init_nvm()


class AESEmulator:

    # memory address where emulation starts
    INST_ADDRESS    = 0x8000000
    RAM_ADDRESS     = 0x20000000
    IO_ADDRESS      = 0x10000000
    NVM_ADDRESS     = 0x50000000

    PLAINTEXT_ADD   = 0x10000000
    KEY_ADD         = 0x10000020
    CIPHERTEXT_ADD  = 0x10000040

    # self.fault_count = 0

    def __init__(self, target_app_folder_path, record_symbol):
        self.bin_path = target_app_folder + '/bin/aes.bin'
        self.elf_path = target_app_folder + '/bin/aes.elf'
        self.THUMB_CODE = load_code(self.bin_path)

        # Initialize emulator in thumb mode
        self.mu = Uc(UC_ARCH_ARM, UC_MODE_THUMB)

        # map 128kiB of flash memory for the code
        self.mu.mem_map(self.INST_ADDRESS, 128 * 1024)
        # map 8kiB of ram memory
        self.mu.mem_map(self.RAM_ADDRESS, 8 * 1024)
        # map 1kiB for Non Volatile Memory (NVM)
        # mu.mem_map(self.NVM_ADDRESS, 1 * 1024)
        # map 1kiB for IO buffer
        self.mu.mem_map(self.IO_ADDRESS, 1 * 1024)

        # write machine code to flash
        self.mu.mem_write(self.INST_ADDRESS, self.THUMB_CODE)

        record_range = extract_symbol_range(record_symbol, self.elf_path)

        self.mu.hook_add(UC_HOOK_CODE, self.hook_code, begin=record_range.start, end=record_range.stop)

    # callback for tracing instructions
    def hook_code(self, uc, address, size, user_data):
        # print(">>> Tracing instruction at 0x%x, instruction size = 0x%x" %(address, size))
        self.trace.append(leakage_model(uc))


    # def execute(self, fault_address, plaintext, key):
    def execute(self, plaintext, key):
        global nvm_content
        self.fault_count = 0
        self.injection_history = dict()
        self.trace = []
        try:

            #write inputs
            self.mu.mem_write(self.PLAINTEXT_ADD,plaintext)
            self.mu.mem_write(self.KEY_ADD, key)
            # mu.mem_write(self.INPUT_ADD, bytes([3,1,4,1]))

            # emulate machine code until timeout
            # Note we start at INST_ADDRESS | 1 to indicate THUMB mode.
            # mu.emu_start( start_address, end_address, timeout, nb_steps)
            self.mu.emu_start(self.INST_ADDRESS | 1, extract_symbol_address("_exit", self.elf_path), 100000, 150000)
            ciphertext = self.mu.mem_read(self.CIPHERTEXT_ADD, 16)

            return (ciphertext, self.trace)

        except UcError as e:
            # print("ERROR: %s" % e)
            return bytes([])

    def __del__(self):
        # map 128kiB of flash memory for the code
        self.mu.mem_unmap(self.INST_ADDRESS, 128 * 1024)
        # map 8kiB of ram memory
        self.mu.mem_unmap(self.RAM_ADDRESS, 8 * 1024)
        # map 1kiB for Non Volatile Memory (NVM)
        # mu.mem_unmap(self.NVM_ADDRESS, 1 * 1024)
        # map 1kiB for IO buffer
        self.mu.mem_unmap(self.IO_ADDRESS, 1 * 1024)
 

def aes_check(plaintext, key):
    alg = AES.new(key, AES.MODE_ECB)
    return alg.encrypt(plaintext)

def differentiate(a, b):
    return bytes([i ^ j for (i, j) in zip(a, b)])

if __name__ == '__main__':
    target_app_folder = '.'
    
    if len(sys.argv) < 3:
        print("Usages: python3 M-CPA.py [target] [exe_count] ([symbol_name])")
        exit()
        
    target_app_folder = sys.argv[1] # target
    exe_count = int(sys.argv[2]) # execution count

    record_symbol = "SubBytes"
    if len(sys.argv) > 3:
        record_symbol = sys.argv[3]

    target_app_folder = os.path.abspath(target_app_folder)
    elf_path = target_app_folder + '/bin/aes.elf'

    # key = secrets.token_bytes(16)
    key = parse_input_bytes("2b7e151628aed2a6abf7158809cf4f3c")
    em = AESEmulator(target_app_folder, record_symbol)

    plaintexts = []
    ciphertexts = []
    traces = []

    for i in range(exe_count):
        if (i+1)%10 == 0:
            print("\r%i/%i" %(i+1, exe_count), end="")
        plaintext = secrets.token_bytes(16)
        expected = aes_check(plaintext, key)
        (ciphertext, trace) = em.execute(plaintext, key)

        plaintexts.append(bytearray(plaintext))
        ciphertexts.append(ciphertext)
        traces.append(trace)

    print("\r")

    # set all the traces to the same size
    tlen = max(map(len, traces))
    nptraces=np.array([xi+[0]*(tlen-len(xi)) for xi in traces]).astype(int)

    # nptraces = np.array(traces, dtype=int)
    npcts    = np.asarray(ciphertexts)
    nppts    = np.asarray(plaintexts)

    np.save("traces.npy", nptraces, allow_pickle=False)
    np.save("ciphertexts.npy", npcts)
    np.save("plaintexts.npy", nppts)



        

    
