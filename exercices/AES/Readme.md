# Résister aux attaques physiques

Exécuter une application avec des fonctions de sécurité sur un système embarqué demande de faire face à une nouvelle menace: les attaques physiques.
L'attaquant a un accès physique au système faisant tourner votre application, ce qui lui offre de nombreuses possibilités: mesurer des temps d'exécution, écouter l'environnement électromagnétique, ou interagir directement avec le circuit ...


## Votre mission

Vous avez été chargé d'auditer un chiffrement AES sur un microcontrôleur. Votre rôle est de prouver la faiblesse de l'implémentation en récupérant la clé de chiffrement.

## Mise en place de l'environnement de travail

Avant de coder, il va falloir mettre en place l'environnement de travail:

- une chaine de cross-compilation permettant de compiler le code pour le microcontrôleur depuis votre pc.
- *python* et certaines bibliothèques qui seront utilisés pour émuler la puce et simuler les attaques.
- à vous d'utiliser votre éditeur de code favoris (emacs bien sûr).

### Linux

#### Installation automatique (recommandée)

Installez le gestionnaire de paquets Nix (nécessite les permissions sudo, et *curl*).

```
curl -L https://nixos.org/nix/install | sh
```

Suivre les indications à l'écran pour finaliser l'installation.

Utiliser le fichier *exercices/default.nix* comme configuration Nix, par exemple

```
cd exercices
nix-shell
```
Vous donne un shell bash avec tout ce qu'il faut pour réaliser les exercices, et sans modifier votre système (en particulier pas besoin de virtualenv pour python).

(default.nix est le fichier par défaut recherché par nix-shell)

#### Installation manuelle (parce que vous ne voulez vraiment pas, mais vraiment pas utiliser Nix)

Commençons par installer la chaine de cross-compilation (commandes données avec apt pour Debian-like).
```
sudo apt install gcc-arm-none-eabi
```

Puis python, son gestionnaire de paquet (pip) et virtualenv.
```
sudo apt install python3
sudo apt install python3-pip
pip3 install virtualenv
```

Nous allons maintenant mettre en place un environnement virtualenv (il faut que l'exécutable soit dans PATH).
```
virtualenv exosenv
```

Enfin, avant chaque session de travail, il faut activer l'environnement:

```
source exosenv/bin/activate
```

**À la fin de la session**, l'environnement est désactivé à l'aide de

```
deactivate
```


Au sein d'une session (après le source, avant le deactivate), installer les bibliothèques python.
```
pip3 install unicorn
pip3 install numpy
pip3 install pyelftools
pip3 install termcolor
pip3 install gmpy2
pip3 install matplotlib
pip3 install pycryptodome
```

## Déroulé de la formation

L'application à auditer se trouve dans le dossier **[aes_encrypt](./aes_encrypt)**.
Si nécessaire, vous pouvez vous référer au standard de l'AES [présent dans ce dossier](./NIST.FIPS.197.pdf).

Il y a deux leçons qui correspondent à deux manières de retrouver la clé.
Dans la leçon **[L5](./L5)**, vous devrez récupérer la clé à l'aide de la consommation de courant.
Dans la leçon **[L6](./L6)**, vous utiliserez des injections de fautes.

Pour chaque leçon, suivre les instructions dans le **Readme.md** du dossier de la leçon.



## Compilation de l'application

Pour pouvoir exécuter l'application, via un émulateur piloté en Python, il va d'abord falloir générer l'application elle-même.
Ouvrez le dossier de votre application de vérification de code PIN: **aes_encrypt**.
```
cd aes_encrypt
```

Votre application sera exécutée sur un microcontrôleur, il faut donc réaliser une compilation croisée. C'est-à-dire que l'on compile notre programme pour un jeu d'instruction différent de celui de la machine hôte, qui compile le code. Vous voulez compiler pour le jeu d'instruction Thumb des microcontrôleurs ARM Cortex-M.

Pour cela vous devez avoir la chaine de compilation (*toolchain* en anglais) 'arm-none-eabi'.
Si votre environnement de travail est bon, il suffit d'exécuter
```
make
```
dans le dossier **aes_encrypt**.

## Tester votre implémentation

Le microcontrôleur sera émulé, c'est-à-dire que vous utiliserez votre machine pour en simuler le fonctionnement.
Pour cela, l'outil [Unicorn](https://www.unicorn-engine.org/) sera utilisé, via son wrapper Python.

Pour tester votre implémentation, lancez le script Python *J-test-aes.py* dans le dossier de l'exercice.
Essayez !

Placez-vous dans le dossier **aes_encrypt** de votre implémentation.

```
python3 ../L5/J-test-aes.py .
```

Le dernier point, solitaire, est nécessaire, car il spécifie le répertoire courant comme étant le répertoire de l'application à utiliser (l'application est le fichier **aes_encrypt/bin/aes.bin**).
Ce script réalise des chiffrements et retourne la clé utilisée, le texte clair, le texte chiffré donné par le microcontrôleur, le texte chiffré issu d'une bibliothèque AES python de référence ainsi que la différence des deux chiffrés. Cette différence doit être nulle.

```
Key                              Plaintext                        Ciphertext                       Expected                         Diff
cc8eb9a2829a291c7f04019c68b5477c aa8f379fef5045637e263245ae285909 30087cfdedcdaa88b63241c883045cf1 30087cfdedcdaa88b63241c883045cf1 00000000000000000000000000000000
7bed9486d7a879c9f2e9d67508cd8cf2 ebe533301e5309efbed4b3c65b791b59 62b846758de379bdc14aa4c9788b08f0 62b846758de379bdc14aa4c9788b08f0 00000000000000000000000000000000
e6568e05ebef624a2933ed27277f6146 89ca9f74423fcd2b11bbdd8e893ddc33 125580108be0451a9de71d6b6fea3242 125580108be0451a9de71d6b6fea3242 00000000000000000000000000000000
c6c80c70799682dad7171e2ed64a76a1 05f197e73050c877684768874b33d03d fcd5b1facd94ba57dbfba11782e33c37 fcd5b1facd94ba57dbfba11782e33c37 00000000000000000000000000000000
a104280d1eb9344c5eae8203916cef00 e5c78898781374c47dd19fe77b677236 a4ce6002509e033cceaad432533e3095 a4ce6002509e033cceaad432533e3095 00000000000000000000000000000000
b18ac74daf93133347153d54992e7969 8bc0e521a894580d753a6bcc2eb7e576 ba1e9a056c862a3e3986641b3a9bf4c5 ba1e9a056c862a3e3986641b3a9bf4c5 00000000000000000000000000000000
0043b1a538793608279c5daff907d47e b8c9913c1c29f12d812106ab781bb067 6ea529e5f31cc349c67bf7f34a3ea271 6ea529e5f31cc349c67bf7f34a3ea271 00000000000000000000000000000000
865463380ddfd401432ffe906e5052e8 f79b8705ec9e25b883f901e89bfc35f1 a4f77457b4b45a26107a63b45bb2b8f6 a4f77457b4b45a26107a63b45bb2b8f6 00000000000000000000000000000000
7d66189b02d330a4800731fe2a2e6c85 319a03439a0aebfe57481dea5da9ebab fa5ad15cff1b6a2d7d0afee08ea0d80a fa5ad15cff1b6a2d7d0afee08ea0d80a 00000000000000000000000000000000
42fc04b17f3517fd4bafac095c34608d 03f8bb2cd2aa7ac8b7aaf7d596da1dd9 be2e383b3ab122a305e587567c78b4be be2e383b3ab122a305e587567c78b4be 00000000000000000000000000000000
```
